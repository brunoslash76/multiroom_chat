module.exports.cadastro = function(application, req, res){
    res.render('cadastro', { validacao: {}, dadosForm: {}, msg : {} } );
}// fim cadastro

module.exports.cadastrar = function(application, req, res){
    
    var dadosFormS = req.body;

    console.log("1- deu certo! cadastrar controller");

    req.assert('nome','Nomé é obrigatório').notEmpty();
    req.assert('sobrenome','Sobrenome é obrigatório').notEmpty();
    req.assert('email','Email é obrigatório').notEmpty();
    req.assert('email','Email não válido').isEmail();
    req.assert('senha','Senha é obrigatória').notEmpty();
    req.assert('confirmar_senha','Confirmar a senha é necessário').notEmpty();

    var errors = req.validationErrors();

    if(dadosFormS.senha != dadosFormS.confirmar_senha){
        res.render('cadastro', {validacao: {msg: 'As senhas tem que ser iguais para confirmar cadastro'}} );
    }

    if(errors){
        res.render('cadastro', { validacao: errors, dadosForm : dadosFormS, msg : 'Cadastro não efetuado' } );
        return;
    }// fim if

    var connection = application.config.dbConnection;
    var CadastroDAO = new application.app.models.CadastroDAO(connection);


    var verificaEmail = CadastroDAO.verificaEmail(dadosFormS);
    

    if(verificaEmail) {

        var UsuarioDAO = new application.app.models.UsuarioDAO(connection);
        UsuarioDAO.inserirUsuario(dadosFormS);
        res.render("index", {validacao : {} , msg: 'S'} );

    } else {

        console.log("2 - entrou no ELSE - cadastro ctrl");
        res.render("cadastro", {validacao : {}, dadosForm : dadosFormS, msg : 'N' });

    }
}// fim cadastrar