module.exports.chat = function(app, req, res){

}// fim do chat


module.exports.iniciaChat = function(app, req, res){

    var dadosForm = req.body;

    req.assert('apelido', 'Apelido é obrigatório').notEmpty();
    req.assert('senha', 'É preciso informar uma senha').notEmpty();

    var erros = req.validationErrors();

    if(erros){
        /*
            Como estamos utilizando o método send da response,
            não é necessário incluir um [return;] para para o fluxo 
            da aplicação.
        */
        res.render('index', {validacao : erros, msg : {} });
        return; // só por precaução XD
    }// fim do if

    /**
     * A função [emit()] faz parte do framework socket.io
     * ele envia ou emite uma variável, que quando recebida 
     * pelo ouvinte dentro de nosso projeto, ele executa 
     * alguma ação.
     * 
     * param msgParaCliente
     */
    app.get('io').emit(
        'msgParaCliente',
        {
            apelido : dadosForm.apelido,
            mensagem : ' acabou de entrar no chat',
        }
    );

    /**
     * O JSON enviado juntamente, recupera os dados de quem enviou 
     * a msg.
     */
    res.render( "chat", { dadosForm : dadosForm } );
}// fim da func 

module.exports.sair = function(application, req, res){ 
    req.session.destroy(function(err){
        res.render('index', {validacao: {},  msg : 'A'} );
    });
}// fim 