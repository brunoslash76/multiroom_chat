function CadastroDAO(connection){

    this._connection = connection();

}

/**
 * O método veridicaEmail faz justamente o que o nome indica
 * verifica se já existe o email que o usuário pretende cadastrar.
 * Se não, ele instancia um objeto da classe UsuárioDAO, insere 
 * os dados do usuário e retorna true para controle da classe 
 * cadastro_ctrl.js, caso contrário ele retorna false.
 */
CadastroDAO.prototype.verificaEmail = function( novo_usuario ){

    var email = novo_usuario.email;
    var existe = false;
    var resultado = [];

    console.log("Entrou na função Verifica Email: valor do email: " + email);

    this._connection.open( function(err, mongocliente ) {

        mongocliente.collection( "usuarios", function( err, collection ){

            collection.find({ email: {$eq: email} }, function( err, result ){
                resultado = result;
                mongocliente.close(); 
            });// fim find 
        });// fim do collection
    });// fim do open

    if( resultado[0] == undefined ){
                    
        existe = true;
        console.log("Fez a verificação email não existe var existe = true");  
        
    }// fim if

    return existe;
}// fim da func

module.exports = function(){
    return CadastroDAO;
}

