// importa o módulo de criptografia
var crypto = require('crypto');

function UsuarioDAO(connection) {
    this._connection = connection();
}// fim da classe

UsuarioDAO.prototype.inserirUsuario = function (usuario) {
    this._connection.open(function (error, mongoclient) {
        mongoclient.collection("usuarios", function (error, collection) {
            var senha_criptografada = crypto.createHash("md5").update(usuario.senha);
            usuario.senha = senha_criptografada;
            collection.insert(usuario);
            mongoclient.close();
        });
    });
}// fim do inserir 

UsuarioDAO.prototype.autenticar = function (usuario, req, res) {
    this._connection.open(function (error, mongoclient) {
        mongoclient.collection('usuarios', function (error, collection) {
            var senha_criptografada = crypto.createHash("md5").update(usuario.senha);
            usuario.senha = senha_criptografada;
            collection.find(usuario.email).toArray(function (error, result) {
                if (result[0] != undefined) { // verifica se existe um resultado
                    req.session.autorizado = true;
                    req.session.usuario = result[0].nome;
                }
                if (req.session.autorizado) {
                    res.redirect('chat');
                } else {
                    res.render('index', { validacao: { msg: 'Usuário não cadastrado' } });
                }
            });
            mongoclient.close();
        });
    });
}// fim do autenticar

module.exports = function () {
    return UsuarioDAO;
}