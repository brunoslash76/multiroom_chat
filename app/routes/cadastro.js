module.exports = function(application){

    application.get('/cadastro', function(req, res){
        application.app.controllers.cadastro_ctrl.cadastro(application, req, res);
    });

    application.post('/cadastrar', function(req, res){
        application.app.controllers.cadastro_ctrl.cadastrar(application, req, res);
    });
}